import socket
from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app, resources={r"/calc": {"origins": "*"}})

@app.route('/calc', methods=['POST'])
def calc():
    try:
        num1 = float(request.json['num1'])
        num2 = float(request.json['num2'])
        result = num1 * num2
        server_name = socket.gethostname()
        return jsonify({'result': result, 'server_name': server_name})
    except Exception as e:
        return jsonify({'error': 'Некорректные данные'}), 400

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)