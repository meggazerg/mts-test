const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.post('/calculate', (req, res) => {
  const num1 = req.body.num1;
  const num2 = req.body.num2;

  import('node-fetch').then(({ default: fetch }) => {
    fetch('http://backend01:5000/calc', {
      method: 'POST',
      body: JSON.stringify({ num1, num2 }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(additionResult => {
        fetch('http://backend02:5000/calc', {
          method: 'POST',
          body: JSON.stringify({ num1, num2 }),
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(response => response.json())
          .then(multiplicationResult => {
            res.json({
              additionResult: additionResult.result,
              additionServer: additionResult.server_name,
              multiplicationResult: multiplicationResult.result,
              multiplicationServer: multiplicationResult.server_name
            });
          })
          .catch(error => {
            console.error('Ошибка умножения:', error);
            res.status(500).json({ error: 'Ошибка умножения' });
          });
      })
      .catch(error => {
        console.error('Ошибка сложения:', error);
        res.status(500).json({ error: 'Ошибка сложения' });
      });
  });
});

app.listen(port, () => {
  console.log(`Сервер запущен на порту ${port}`);
});
